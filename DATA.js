export var DATA = [
  {
    id: 'c1b1',
    title: 'Journeys',
    author: 'Max James',
    pages: '512',
    pagesRead: '216'
  },
  {
    id: 'c605',
    title: 'Going dark',
    author: 'Julia Felix',
    pages: '305',
    pagesRead: '105'
  },
  {
    id: '3da1',
    title: 'Wolfsmen',
    author: 'Sandra Marcus',
    pages: '1005',
    pagesRead: '891'
  },
];
