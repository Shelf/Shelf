import 'react-native-gesture-handler';
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { Button, Image, SafeAreaView, Text, View } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer, getFocusedRouteNameFromRoute } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Ionicons, Feather } from '@expo/vector-icons';

import { HomeScreen } from './screens/HomeScreen';
import { SettingsScreen } from './screens/SettingsScreen';
import { StatsScreen } from './screens/StatsScreen';
import { WishlistScreen } from './screens/WishlistScreen';
import { styles } from './styles/stylesheet';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName='Root'
        options={({ route }) => ({
          //headerTitle: getHeaderTitle(route),
          //headerStyle: {backgroundColor: '#333030'},
          //headerTintColor: '#fff'
          //headerTitleStyle: {fontWeight: 'bold'},
        })}
        >
        <Stack.Screen
          name="Root"
          component={Root}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

function Root() {
  return (
    <Tab.Navigator
      initialRouteName='Home'
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;

          if (route.name === 'Home') {
            iconName = focused
              ? 'library'
              : 'library-outline';
          } else if (route.name === 'Wishlist') {
            iconName = focused ? 'list' : 'list-outline'
          } else if (route.name === 'Stats') {
            iconName = focused ? 'timer' : 'timer-outline'
          } else if (route.name === 'Settings') {
            iconName = focused ? 'settings-sharp' : 'settings-outline'
          }
          return <Ionicons name={iconName} size={size} color={color}/>;
        },
      })}
      tabBarOptions={{
        activeTintColor: '#e8bc70',
        inactiveTintColor: 'grey',
        style: { backgroundColor: '#333030' },
      }}
    >
      <Tab.Screen
        name='Home'
        component={HomeScreen}
        options={{ title: 'Shelf' }}
      />
      <Tab.Screen
        name='Wishlist'
        component={WishlistScreen}
      />
      <Tab.Screen
        name='Stats'
        component={StatsScreen}
        options={{ bookId: null }}
      />
      <Tab.Screen
        name='Settings'
        component={SettingsScreen}
      />
    </Tab.Navigator>
  );
}

function getHeaderTitle(route) {
  // If the focused route is not found, we need to assume it's the initial screen
  // This can happen during if there hasn't been any navigation inside the screen
  // In our case, it's "Feed" as that's the first screen inside the navigator
  const routeName = getFocusedRouteNameFromRoute(route) ?? 'Home';

  switch (routeName) {
    case 'Home':
      return 'Home';
    case 'Wishlist':
      return 'Wishlist';
    case 'Stats':
      return 'Stats';
    case 'Settings':
      return 'Settings';
  }
}
