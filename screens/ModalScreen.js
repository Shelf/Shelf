import React, { useState } from 'react';
import { Button, Image, Modal, Pressable, Switch, Text, View } from 'react-native';
import { ModalStyles, Styles, Typography } from '../styles/stylesheet';
import { Feather } from '@expo/vector-icons';

export const CustomModal = (props) => {
  return (
  <Modal
    presentationStyle='pageSheet'
    animationType='slide'
    visible={props.visible}
    onRequestClose={props.onRequestClose}
  >
    <View style={ModalStyles.modalContainer}>
      <View style={ModalStyles.modalHeader}>
        <Text style={Typography.textTitle}>{props.title}</Text>
          <Feather
            style={[Styles.hitbox, ModalStyles.modalHeaderPressable]}
            name='x'
            size={25}
            color='white'
            onPress={props.toggleVisible}
          />
      </View>
      <View style={ModalStyles.modalContentContainer}>
        {props.children}
      </View>
    </View>
  </Modal>
)};

export const DetailModal = (props) => {
  return (
    <Modal
      animationType='slide'
      transparent={true}
      visible={props.visible}
      onRequestClose={props.onRequestClose}
    >
      <View style={ModalStyles.detailModalContainer}>
        <View style={ModalStyles.detailModal}>
          <View style={ModalStyles.modalHeader}>
            <Text style={[Typography.textTitle, {color:'black'}]}>{props.title}</Text>
            <Feather
              style={[Styles.hitbox, ModalStyles.modalHeaderPressable]}
              name='x'
              size={25}
              color='black'
              onPress={props.toggleVisible}
            />
          </View>
          <View style={ModalStyles.detailModalContentContainer}>
            {props.children}
          </View>
        </View>

      </View>
    </Modal>
)}
