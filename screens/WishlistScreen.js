import React, { useState } from 'react';
import { Button, Image, FlatList, Modal, Pressable, Switch, Text, TouchableOpacity, SafeAreaView, View } from 'react-native';
import { Styles, Typography } from '../styles/stylesheet';
import { CustomModal } from '../screens/ModalScreen';
import { Feather } from '@expo/vector-icons';

export function WishlistScreen({navigation, route}){
  const renderItem = ({ item }) => (
    <Item title={item.title} author={item.author} />
  );

  const [filterModalVisible, setFilterModalVisible] = useState(false);
  const [addModalVisible, setAddModalVisible] = useState(false);

  const [buttonPressed, setButtonPressed] = useState(false);

  const [sortValue, setSortValue] = useState('Title');
  const [showUnread, setShowUnread] = useState(true);
  const [showRead, setShowRead] = useState(1);

  return (
    <SafeAreaView style={Styles.container}>
      <View style={Styles.headerContainer}>
        <Image
          style={Styles.headerLogo}
          source={require('../assets/header.png')}
        />
        <Pressable
          style={Styles.headerButton}
          hitSlop={5}
          onPressIn={() => {setAddModalVisible(!addModalVisible)}}
        >
          <Feather
            name='plus'
            style={Styles.headerButtonIcon}
            size={25}
          />
        </Pressable>
        <Pressable
          style={Styles.headerButton}
          hitSlop={5}
          onPressIn={() => {setFilterModalVisible(!filterModalVisible)}}
        >
          <Feather
            name='filter'
            style={Styles.headerButtonIcon}
            size={25}
          />
        </Pressable>
      </View>
      <View style={Styles.contentContainer}>
        <FlatList
          data={DATA.sort((a, b) => a.title.localeCompare(b.title))}
          renderItem={renderItem}
          keyExtractor={item => item.id}
          horizontal={false}
          numColumns={3}
        />
      </View>
      <CustomModal
        title='Filter'
        presentationStyle='pageSheet'
        animationType="slide"
        visible={filterModalVisible}
        toggleVisible={()=>{setFilterModalVisible(!setFilterModalVisible)}}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setFilterModalVisible(!filterModalVisible);
        }}
      >
        <TouchableOpacity
          style={[Styles.hitbox,Styles.filterOptions]}
          onPress={() => {setSortValue(sortValue == 'Title' ? 'Author' : 'Title')}}>
            <Text style={Styles.filterOptionsText}>{sortValue}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[Styles.hitbox, Styles.filterOptions]}
          onPress={() => {setShowRead(!showRead)}}
        >
          <Text style={[Styles.filterOptionsText,
            {color: showRead ? '#fff' : '#aaa',
            fontWeight: showRead ? 'bold' : 'normal'}]}>Read</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[Styles.hitbox,Styles.filterOptions]}
          onPress={() => {setShowUnread(!showUnread)}}
        >
          <Text style={[ Styles.filterOptionsText,
            {color: showUnread ? '#fff' : '#aaa',
            fontWeight: showUnread ? 'bold' : 'normal'}]}>Unread</Text>
        </TouchableOpacity>
      </CustomModal>
      <CustomModal
      title='Add to Library'
      presentationStyle='pageSheet'
      animationType="slide"
      visible={addModalVisible}
      toggleVisible={()=>{setAddModalVisible(!addModalVisible)}}
      onRequestClose={() => {
        Alert.alert("Modal has been closed.");
        setAddModalVisible(!addModalVisible);
      }}
      >
    </CustomModal>
    </SafeAreaView>
  );
};

const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    title: 'Journeys',
    author: 'Max James',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'Going dark',
    author: 'Julia Felix',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    title: 'Wolfsmen',
    author: 'Sandra Marcus',
  },
];

const Item = ({ title, author }) => (
  <View style={Styles.item}>
    <Image style={Styles.itemIcon} source={require('../assets/graphics/book_blue_300.png')}/>
    <Text style={Styles.itemTitle}>{title+' -'}</Text>
    <Text style={Styles.itemTitle}>{author}</Text>
  </View>
);
