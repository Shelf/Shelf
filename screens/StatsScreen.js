import React, { useState } from 'react';
import { Alert, Button, Image, Pressable, Text, SafeAreaView, View } from 'react-native';
import { Feather } from '@expo/vector-icons';
import { Styles, Typography } from '../styles/stylesheet.js'
import { CustomModal } from '../screens/ModalScreen';
import { DATA } from '../DATA';

export function StatsScreen({navigation, route}){
  const [modalVisible, setModalVisible] = useState(false);
  const [recordingActivated, setRecordingActivated] = useState(false);

  var sec = 0; var min = 0; var hrs = 0;
  var secString = sec < 10 ? '0'+sec.toString() : sec.toString();
  var minString = min < 10 ? '0'+min.toString() : min.toString();
  var hrsString = hrs < 10 ? '0'+hrs.toString() : hrs.toString();
  var time = hrsString+':'+minString+':'+secString;


{/*
  var currentTimer = null;

  const manageClock = (currentTimer) => {
    Alert.alert(currentTimer)
    if (currentTimer == null) {
      currentTimer = startClock(currentTimer);
    } else {
      stopClock(currentTimer)
    }
  };

  const startClock = (currentTimer) => {
    //var startTime = new Date();
    var currentTimer = setInterval(()=>{}, 1000);
    Alert.alert('Your time reading is being tracked. Enjoy your time reading.'+currentTimer);
    return (currentTimer) };

  const stopClock = (currentTimer) => {
    clearInterval(currentTimer);
    currentTimer = null;
    Alert.alert('Nothing beats a good book.');
  };

  const countTime = (startTime) => {
    currentTime = new Date();
    timeReading = (currentTime - startTime).getSeconds();
  }
*/}

  return (
    <SafeAreaView style={Styles.container}>
      <View style={Styles.headerContainer}>
        <Image
          style={Styles.headerLogo}
          source={require('../assets/header.png')}
        />
        <Pressable
          style={[{backgroundColor: '#717275'},Styles.statsHeaderButton]}
          hitSlop={5}
          onPressIn={() => {navigation.setParams({bookId: undefined})}}
        >
          <Feather
            name='award'
            style={Styles.headerButtonIcon}
            size={25}
          />
        <Text style={Styles.statsHeaderButtonText}>General Stats</Text>
        </Pressable>
        <Pressable
          style={[{backgroundColor: recordingActivated ? '#b25f5f' : '#5f83af'},Styles.statsHeaderButton]}
          hitSlop={5}
          onPressIn={() => {setRecordingActivated(!recordingActivated); {/*manageClock(currentTimer)*/}}}
        >
          <Feather
            name={recordingActivated ? 'stop-circle' : 'clock'}
            style={Styles.headerButtonIcon}
            size={25}
          />
          <Text style={Styles.statsHeaderButtonText}>{recordingActivated ? 'Stop   '+time : 'Start reading'}</Text>
        </Pressable>
        <Text style={Styles.headerTitle}>
          {(route.params === undefined || DATA[route.params.bookId] === undefined) ?
          'General Stats' : DATA[route.params.bookId].title}
        </Text>

      </View>


      <View style={Styles.contentContainer}>
        <View style={[Styles.statsContentContainer, Styles.statsContentCotainerTop]}>
          <View style={Styles.statsContentContainerStats}>
            <View style={Styles.statsContentCotainerTopContent}>
              <Text style={Styles.statsContentContainerHeading}>Stats</Text>
                {(route.params === undefined || DATA[route.params.bookId] === undefined) ?
                  <View>
                    <Text style={Styles.statsDetail}>
                      ??? minutes read this month
                    </Text>
                    <Text style={Styles.statsDetail}>
                      ??? pages read
                    </Text>
                  </View>
                :
                  <View>
                    <Text style={Styles.statsReadPercentage}>
                      {Math.round(DATA[route.params.bookId].pagesRead / DATA[route.params.bookId].pages * 100)}%
                    </Text>
                    <Text style={Styles.statsDetail}>
                      {DATA[route.params.bookId].pagesRead} /  {DATA[route.params.bookId].pages} pages read
                    </Text>
                  </View>
                }
            </View>
          </View>
          <Image style={Styles.statsContentIcon} source={require('../assets/graphics/book_blue_300.png')}/>
        </View>
        <View style={[Styles.statsContentContainer, Styles.statsContentCotainerBottom]}>
        </View>
      </View>
      <CustomModal
        title="StatsModal"
        presentationStyle='pageSheet'
        animationType='slide'
        visible={modalVisible}
        toggleVisible={()=>{setModalVisible(!setModalVisible)}}
        onRequestClose={()=>{}}
      >
        <Text>Hallo, das ist ein Test</Text>
      </CustomModal>
    </SafeAreaView>
  );
};
