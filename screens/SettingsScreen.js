import React, { useState } from 'react';
import { Button, Image, FlatList, Modal, Pressable, Switch, Text, TouchableOpacity, SafeAreaView, View } from 'react-native';
import { Styles, Typography } from '../styles/stylesheet';
import { ModalContainer } from '../screens/ModalScreen';
import { Feather } from '@expo/vector-icons';

export function SettingsScreen(){
  return (
    <SafeAreaView style={Styles.container}>
      <View style={Styles.headerContainer}>
        <Image
          style={Styles.headerLogo}
          source={require('../assets/header.png')}
        />
      </View>
      <View style={Styles.contentContainer}>
        <Text style={Typography.text}>Settings Screen</Text>
      </View>
    </SafeAreaView>
  );
};
