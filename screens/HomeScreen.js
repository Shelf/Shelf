import React, { useEffect, useState } from 'react';
import { Button, Image, FlatList, Modal, Pressable, Switch, Text, TextInput, TouchableOpacity, SafeAreaView, StyleSheet, View } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { Typography, Styles, ModalStyles } from '../styles/stylesheet';
import { CustomModal, DetailModal } from '../screens/ModalScreen';
import { DATA } from '../DATA';
import { Feather } from '@expo/vector-icons';
import { BarCodeScanner } from 'expo-barcode-scanner';


export function HomeScreen({navigation, route}){
  const Item = ({ id, title, author }) => (
    <View style={Styles.item}>
      <Pressable
        style={Styles.itemPressable}
        onPress={()=>{setDetailModalVisible(!detailModalVisible), setSelectedBookId(getBookInfo(id))}}
        onLongPress={() => {navigation.navigate('Stats', {bookId:getBookInfo(id)})}}
      >
        <Image style={Styles.itemIcon} source={require('../assets/graphics/book_blue_300.png')}/>
      </Pressable>

      <Text style={Typography.textDefault}>{title+' -'}</Text>
      <Text style={Typography.textDefault}>{author}</Text>
    </View>
  );

  const renderItem = ({ item }) => (
    <Item title={item.title} author={item.author} id={item.id}/>
  );

  const getBookInfo = ( id ) => (DATA.findIndex(book => book.id === id));

  const [filterModalVisible, setFilterModalVisible] = useState(false);
  const [addModalVisible, setAddModalVisible] = useState(false);
  const [detailModalVisible, setDetailModalVisible] = useState(false);

  const [buttonPressed, setButtonPressed] = useState(false);

  const [sortValue, setSortValue] = useState('Title');
  const [showUnread, setShowUnread] = useState(true);
  const [showRead, setShowRead] = useState(1);

  const [selectedBookId, setSelectedBookId] = useState(0);

  const [bookIsbnAdd, setBookIsbnAdd] = useState(null);
  const [bookTitleAdd, setBookTitleAdd] = useState(null);
  const [bookAuthorAdd, setBookAuthorAdd] = useState(null);

  /* prerequisites for scannin barcodes */
  const [scannerModalVisible, setScannerModalVisible] = useState(false);
  const [hasPermissionCamera, setHasPermissionCamera] = useState(null);
  const [scannedBarcode, setScannedBarcode] = useState(false);

  useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermissionCamera(status == 'granted');
    })();
  }, []);

  const handleBarCodeScanned = ({ type, data }) => {
    setScannedBarcode(true);
    alert('Barcode with type '+type+' and data '+data+' has been scanned!')
  };

  if (hasPermissionCamera === null) {
    return <Text>Requesting fpr camera permission..</Text>;
  }
  if (hasPermissionCamera === false) {
    return <Text>No access to camera.</Text>;
  }

  return (
    <SafeAreaView style={Styles.container}>
      <View style={Styles.headerContainer}>
        <Image
          style={Styles.headerLogo}
          source={require('../assets/header.png')}
        />
        <Pressable
          style={Styles.headerButton}
          hitSlop={5}
          onPressIn={() => {setAddModalVisible(!addModalVisible)}}
        >
          <Feather
            name='plus'
            style={Styles.headerButtonIcon}
            size={25}
          />
        </Pressable>
        <Pressable
          style={Styles.headerButton}
          hitSlop={5}
          onPressIn={() => {setFilterModalVisible(!filterModalVisible)}}
        >
          <Feather
            name='filter'
            style={Styles.headerButtonIcon}
            size={25}
          />
        </Pressable>
      </View>
      <View style={Styles.contentContainer}>
        <FlatList
          data={DATA.sort((a, b) => a.title.localeCompare(b.title))}
          renderItem={renderItem}
          keyExtractor={item => item.id}
          horizontal={false}
          numColumns={3}
        />
      </View>
      <CustomModal
        title='Filter'
        visible={filterModalVisible}
        toggleVisible={()=>{setFilterModalVisible(!setFilterModalVisible)}}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setFilterModalVisible(!filterModalVisible);
        }}
      >
        <TouchableOpacity
          style={[Styles.hitbox,Styles.filterOptions]}
          onPress={() => {setSortValue(sortValue == 'Title' ? 'Author' : 'Title')}}>
            <Text style={Styles.filterOptionsText}>{sortValue}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[Styles.hitbox, Styles.filterOptions]}
          onPress={() => {setShowRead(!showRead)}}
        >
          <Text style={[Styles.filterOptionsText,
            {color: showRead ? '#fff' : '#aaa',
            fontWeight: showRead ? 'bold' : 'normal'}]}>Read</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[Styles.hitbox,Styles.filterOptions]}
          onPress={() => {setShowUnread(!showUnread)}}
        >
          <Text style={[ Styles.filterOptionsText,
            {color: showUnread ? '#fff' : '#aaa',
            fontWeight: showUnread ? 'bold' : 'normal'}]}>Unread</Text>
        </TouchableOpacity>
      </CustomModal>
      <CustomModal
        title='Add to Library'
        visible={addModalVisible}
        toggleVisible={()=>{setAddModalVisible(!addModalVisible)}}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setAddModalVisible(!addModalVisible);
        }}
      >
        <TextInput
          placeholder="ISBN: ..."
          onChangeText={bookIsbnAdd => setBookTitle(bookIsbnAdd)}
          defaultValue={bookIsbnAdd}
          style={Styles.textInput}
        />
        <TextInput
          placeholder="Title: ..."
          onChangeText={bookTitleAdd => setBookTitle(bookTitleAdd)}
          defaultValue={bookTitleAdd}
          style={Styles.textInput}
        />
        <TextInput
          placeholder="Author: ..."
          onChangeText={bookAuthorAdd => setBookTitle(bookAuthorAdd)}
          defaultValue={bookAuthorAdd}
          style={Styles.textInput}
        />
        <Pressable
          color='#fff'
          style={{backgroundColor: 'green', margin: 50, padding: 20}}
          hitSlop={5}
          onPressIn={() => {
            setAddModalVisible(!addModalVisible),
            setScannerModalVisible(!scannerModalVisible)
          }}
        >
          <Text>Scan a barcode!</Text>
        </Pressable>
        <Pressable
          onPress={() => {}}
          color='#fff'
          style={{backgroundColor: 'green'}}
        >
          <Text>Add to your Shelf!</Text>
        </Pressable>
    </CustomModal>
    <DetailModal
      title='Book details'
      bookId = {() => {getBookInfo(selectedBookId)}}
      visible={detailModalVisible}
      toggleVisible={()=>{setDetailModalVisible(!detailModalVisible)}}
      onRequestClose={() => {
        Alert.alert("Modal has been closed.");
        setDetailModalVisible(!detailModalVisible);
      }}
    >
      <Text style={Typography.textDark}>Title: {DATA[selectedBookId].title}</Text>
      <Text style={Typography.textDark}>Author: {DATA[selectedBookId].author}</Text>

    <Text>{selectedBookId}</Text>
    </DetailModal>
    <DetailModal
      title='Scan a barcode'
      visible={scannerModalVisible}
      toggleVisible={()=>{
        setScannerModalVisible(!scannerModalVisible);
        setAddModalVisible(!addModalVisible)
      }}
      onRequestClose={() => {
        Alert.alert("Modal has been closed.");
        setScannerModalVisible(!scannerModalVisible);
        setAddModalVisible(!addModalVisible);
      }}>
      <BarCodeScanner
        onBarCodeScanned={scannedBarcode ? undefined : handleBarCodeScanned}
        style={{alignSelf: 'center',width: "110%", height: "80%"}}
      />
      {scannedBarcode && <Button title={'Tap to Scan Again'} onPress={() => setScannedBarcode(false)} />}
    </DetailModal>
    </SafeAreaView>
  );
}
