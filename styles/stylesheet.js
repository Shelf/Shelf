import React from 'react';
import { StyleSheet } from 'react-native';


export const Typography = StyleSheet.create({
  textTitle: {
    color: '#fff',
    fontSize: 32,
    marginBottom: 50,
  },
  textDefault: {
    color: '#fff',
    fontSize: 15
  },
  textDark: {
    color: '#333',
  },
});

export const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#333030',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hitbox: {
    padding: 5,
  },
  headerContainer: {
    //backgroundColor: 'green',
    flex: 1,
    //flexDirection: 'column',
    justifyContent: 'space-between',
    //alignItems: 'flex-end',
    width: '100%',
    paddingTop: '5%',
  },
  headerLogo: {
    //flex: 1,
    position: 'absolute',
    left: '3%', // x-50 for centering image
    top: '10%',
    width: '30%',
    zIndex: 1,
  },
  headerTitle: {
    color: '#fff',
    position: 'absolute',
    left: '3%',
    bottom: '0%',
    fontSize: 20,
    fontWeight: 'bold',
  },
  headerButton: {
    right: '-85%',
    height: 35,
    width: 75,
    backgroundColor: '#717275',
    borderRadius: 25,
    //alignItems: 'center',
    justifyContent: 'center',
  },
  statsHeaderButton: {
    right: '-65%',
    height: 35,
    width: 300,
    borderRadius: 25,
    //alignItems: 'flex-start',
    justifyContent: 'center',
  },
  statsHeaderButtonText: {
    position: 'absolute',
    left: 40,
    color: '#fff'
  },
  headerButtonIcon: {
    position: 'absolute',
    color: 'white',
    left: 10,
    //marginRight: 20,
  },

  contentContainer: {
    flex: 6,
    justifyContent: 'space-around',
    alignItems: 'center',
    width: '100%',
  },
  statsContentContainer: {
    //backgroundColor: '#330000',
    width: '90%',
    margin: 'auto',
  },
  statsContentCotainerTop: {
    height: '40%'
  },
  statsContentCotainerTopContent: {
    marginLeft: '25%',
    height: '100%',
  },
  statsContentCotainerBottom: {
    height: '50%',
  },
  statsContentContainerStats: {
    backgroundColor: '#6c6c6c',
    width: '80%',
    height: '80%',
    position: 'absolute',
    bottom: 0, right: 0,
    borderRadius: 10,
  },
  statsContentIcon: {
    //width: ,
    position: 'absolute',
    top: 0, left: '-20%',
    height: '75%',
    width: '75%',
    resizeMode: 'contain',
    //width: 'auto'
  },
  statsContentContainerHeading: {
    color: '#ffffff',
    fontSize: 20,
    fontWeight: 'bold',
    marginVertical: 20,
  },
  statsReadPercentage: {
    marginLeft: 20,
    fontSize: 32,
    color: '#e8bc70',
  },
  statsDetail: {
    marginLeft: 20,
    marginBottom: 10,
    color: '#e8bc70',
  },
  item: {
    //flex: 1,
    //justifyContent: 'space-around',
    alignItems: 'center',
    marginVertical: 25,
    marginHorizontal: 20,
    //backgroundColor: 'purple',
  },
  itemPressable: {
    width: '100%',
  },
  itemIcon: {
    width: '100%',
    height: 100,
    marginVertical: 5,
  },
  itemTitle: {
    color: 'white',
  },

  filterOptions: {
    marginBottom: 15,
  },
  filterOptionsText: {
    //lineHeight: 1,
    fontSize: 18,
    color: '#fff'
  },
  textInput: {
    alignSelf: 'center',
    width: '90%',
    color: '#fff',
    fontSize: 15,
    backgroundColor: '#fff',
    borderRadius: 10,
    padding: 10,
    margin: 10,
  }
});

export const ModalStyles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    backgroundColor: '#333030',
    padding: 25,
  },
  modalContentContainer: {
    flex: 1,
    alignItems: 'flex-start',
  },
  detailModalContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: "center",
  },
  detailModal: {
    //margin: 20,
    padding: 25,
    width: '80%',
    height: '60%',
    backgroundColor: "white",
    borderRadius: 20,
    //justifyContent: 'center',
    //alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  detailModalContentContainer: {
    alignItems: 'flex-start',
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  },
  modalHeaderPressable: {
    position: 'absolute',
    right: 0,
  },
});
