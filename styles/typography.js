import React from 'react';
import { StyleSheet } from 'react-native';

export const typography = StyleSheet.create({
  textTitle: {
    color: '#fff',
    fontSize: 32,
    marginBottom: 50,
  },
  textDefault: {
    color: '#fff',
  },
  textDark: {
    color: '#333',
  },
})
